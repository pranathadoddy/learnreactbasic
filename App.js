import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import PostList from './components/PostList';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, List, ListItem } from 'native-base';


export default class App extends React.Component {

  state={isLoading:true,posts:[{
    title:'Title 1',
    descriiption:'lorem ipsum'
  },{
    title:'Title 2',
    descriiption:'lorem ipsum'
  },{
    title:'Title 3',
    descriiption:'lorem ipsum'
  },{
    title:'Title 4',
    descriiption:'lorem ipsum'
  }]};




async componentWillMount() {

  await Expo.Font.loadAsync({
    'Roboto': require('native-base/Fonts/Roboto.ttf'),
    'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
  });

  this.setState({isLoading:false});
}

  AddNewPosts() {

    let posts=this.state.posts.slice(0);

    posts.push({
      title:'Title 5',
      descriiption:'lorem ipsum'
    });

    this.setState({posts:posts})
  }


  render() {
    if(!this.state.isLoading){
      return (
        <Container>
            <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Header</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <PostList posts={this.state.posts}/>
          <Button onPress={this.AddNewPosts.bind(this)} light><Text>Add New Post</Text></Button>
        </Content>
        <Footer>
          <FooterTab>
            <Button full>
              <Text>Footer</Text>
            </Button>
          </FooterTab>
        </Footer>
     
      </Container>);
    }

    return null;
     
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
