import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, List, ListItem } from 'native-base';

export default class PostList extends React.Component {

  render() {
      return (
            <List>
                {this.props.posts.map((post, index)=>{
                  return( <ListItem key={index} style={{flex:1, flexDirection:'column', alignItems:'flex-start'}}>
                    <View style={{flex:1}}>
                      <Text>{post.title}</Text>
                    </View>
                    <View style={{flex:1}}>
                      <Text>{post.descriiption}</Text>
                    </View>
                  </ListItem>)
                })}
            </List>
      );
    
    }
}


