import React, {Component} from 'react';
import { StyleSheet, Image } from 'react-native';

class ComponentImage extends Component{
    render(){
        let picUri='https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg';

        return (<Image source={{uri:picUri}} resizeMode='stretch' style={{width:'100%', height:'100%'}}/>);
    }

}

export default ComponentImage;